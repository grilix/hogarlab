#!/usr/bin/env bash

SSH_ARGS="-o PasswordAuthentication=no"
REMOTE_TMP="/tmp/hogarlab"

fail() {
  echo "$@"
  exit -1
}

copy-file() {
  scp $SSH_ARGS "$1" "$HOST:$2" >/dev/null
}

fetch-file() {
  scp $SSH_ARGS "$HOST:$1" "$2" >/dev/null || \
    fail "No se pudo obtener $HOST:$1"
}

local-install-kubectl() {
  local VERSION=$(curl -s \
    https://storage.googleapis.com/kubernetes-release/release/stable.txt)

  echo -n "Descargando kubectl: "
  curl -LO \
    https://storage.googleapis.com/kubernetes-release/release/$VERSION/bin/linux/amd64/kubectl \
    >/dev/null 2>&1
  echo "Éxito!"

  echo "Instalando el ejecutable localmente..."
  echo "Necesitarás ingresar la contraseña del usuario local."

  chmod +x ./kubectl
  sudo mv ./kubectl /usr/local/bin/kubectl || fail "No se ha podido instalar kubectl"

  echo "El comando kubectl ha sido instalado!"
}

remote-setup-k3s() {
  local HOST="$1"
  local USER="$2"

  if ! ssh $SSH_ARGS "$HOST" echo; then
    ./bin/diagnose-connection.sh "$HOST"

    exit -1
  fi

  echo "Enviando script de instalación al host ..."
  ssh $SSH_ARGS "$HOST" mkdir -p "$REMOTE_TMP/scripts"
  copy-file "scripts/setup-k3s.sh" "$REMOTE_TMP/scripts"
  echo "Ejecutando instalación remotamente ..."

  if ! ssh $SSH_ARGS "$HOST" -t \
    "$REMOTE_TMP/scripts/setup-k3s.sh" "$USER"; then

    echo "Instalación cancelada."
    exit -1
  fi

  fetch-file "~/.hogarlab/$USER.crt" "keys/$HOST.crt"
  fetch-file "~/.hogarlab/$USER.key" "keys/$HOST.key"
  fetch-file "~/.hogarlab/k8s-ca.crt" "keys/$HOST-ca.crt"
  fetch-file "~/.hogarlab/endpoint.txt" "keys/$HOST-endpoint.txt"

  local CONFIG=$(ssh -G "$HOST")
  local HOSTNAME=$(echo "$CONFIG" | grep '\<hostname ' | sed 's/hostname //')

  sed -i "s/127.0.0.1/$HOSTNAME/" "keys/$HOST-endpoint.txt"
}

download_config() {
  local HOST="$1"
  echo "Necesitamos obtener el archivo con las credenciales de k3s."
  echo "Deberás ingresar la contraseña para este paso."

  ssh $SSH_ARGS "$HOST" -C cat /etc/rancher/k3s/k3s.yaml | \
    sed "s/127.0.0.1/$HOST/" | \
    sudo tee ~/.kube/config
}

if [ -z "$1" ]; then
  fail "
Necesitas pasar el nombre del servidor como parámetro:

  $0 mi-servidor
"
fi

HOST="$1"
USER=$(whoami)

type kubectl >/dev/null 2>&1 || local-install-kubectl

[ -d "keys" ] || mkdir "keys"

if [ ! -f "keys/$HOST.crt" -o \
     ! -f "keys/$HOST.key" -o \
     ! -f "keys/$HOST-ca.crt" -o \
     ! -f "keys/$HOST-endpoint.txt" ]; then

  remote-setup-k3s "$HOST" "$USER"
fi

if ! kubectl config get-clusters | grep "hogarlab-$HOST" >/dev/null; then
  kubectl config set-cluster "hogarlab-$HOST" \
    --embed-certs=true \
    --server="$(cat keys/$HOST-endpoint.txt)" \
    --certificate-authority="keys/$HOST-ca.crt"

  kubectl config set-credentials "$USER" \
    --embed-certs \
    --client-certificate="keys/$HOST.crt" \
    --client-key="keys/$HOST.key"

  kubectl config set-context "$USER-$HOST" \
    --cluster="hogarlab-$HOST" \
    --user="$USER"

  kubectl config use-context "$USER-$HOST"
fi
