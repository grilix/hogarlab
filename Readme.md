# Hogarlab

Para instalar aplicaciones, usaremos k3s a través del comando `kubectl`. Esta herramienta te ayudará a instalar k3s en tu servidor.

### Servidor

El servidor será donde instalaremos nuestras aplicaciones y deberá estar siempre encendido y conectado a la red.

Antes de comenzar, asegúrate que tenga:
- GNU/Linux (debian/ubuntu vendría perfecto).
- Servidor SSH.
- Un usuario "no root" capaz de ejecutar `sudo` (prueba `sudo ls` para verificarlo).

### Cliente

El cliente es el ordenador que usaremos para administrar nuestro servidor. Aquí necesitaremos:

- GNU/Linux (de nuevo, debian/ubuntu sería perfecto).
- Un cliente SSH, configurado para conectarse al servidor usando la clave pública.

    **NOTA**: Si sabes cómo instalar `kubectl` desde un script en MS Windows, aceptamos colaboraciones!

## Instalación

Luego de clonarte este repositorio en tu dispositivo, inicia la instalación con corriendo:

```
./install.sh "<nombre-servidor>"
```

Para asegurarte de que todo está funcionando correctamente, ejecuta:

```
kubectl get all -A
```
