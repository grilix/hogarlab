#!/usr/bin/env bash

# Este script realizará un diagnóstico para determinar por qué
# ha fallado la conexión al host.

error() {
  echo " ****************" >&2
  if [ "$#" == "0" ]; then
    cat >&2
  else
    echo "$@" >&2
  fi
  echo " ****************" >&2

  exit -1
}

if [ -z "$1" ]; then
  echo "<Parámetro +host+ no establecido>"
  exit -1
fi

HOST="$1"

echo "Iniciando diagnóstico de conexión..."

CONFIG=$(ssh -G "$HOST")
HOSTNAME=$(echo "$CONFIG" | grep '\<hostname ' | sed 's/hostname //')
USER=$(echo "$CONFIG" | grep '\<user ' | sed 's/user //')

echo "Configuración en uso:"
echo "  usuario: $USER"
echo "     host: $HOSTNAME"
echo ""

echo -n "Saludando al host: "

RESULT=$(ping -v -w 3 -c 1 "$HOSTNAME" 2>&1)

if [ "$?" != "0" ]; then
  echo "No hubo respuesta."

  echo "Verifica que el host:"
  echo " - Esté encendido."
  echo " - Esté funcionando normalmente."
  echo " - Esté conectado a la red."

  exit -1
fi

echo "El host nos saluda de vuelta!"

echo -n "Probando accesso por SSH: "
RESULT=$(ssh -v -o 'PasswordAuthentication no' "$HOST" echo 2>&1)

if [ "$?" != "0" ]; then
  # TODO: el mensaje puede variar según idioma
  if echo "$RESULT" | grep 'Connection refused' >/dev/null; then
    echo "Conexión no disponible."

    error \
      "El host no acepta conexiones SSH. Es posible que el servicio SSH no" \
      "esté corriendo o el firewall esté bloqueando el acceso."
  fi

  # TODO: el mensaje puede variar según idioma
  if echo "$RESULT" | grep 'Permission denied' >/dev/null; then
    echo "Permiso denegado."

    error \
      "El host ha denegado el acceso. Asegúrate de que has configurado" \
      "correctamente tu par de claves pública/privada."
  fi

  echo "Conexión no permitida."

  error \
    "Ha fallado la conexión por SSH. Asegúrate de que tienes acceso al host" \
    "con el usuario local (`whoami`), y que has configurado tu par de claves."
fi

echo "Conexión permitida!"

