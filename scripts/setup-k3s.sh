#!/usr/bin/env bash

# Este script se ejecutará en el host para realizar la instalación de k3s

SSH_ARGS="-o PasswordAuthentication=no"

fail() {
  echo "$@"
  exit -1
}

install-k3s() {
  echo "Instalando k3s ..."

  curl -sfL https://get.k3s.io |
    INSTALL_K3S_CHANNEL=latest \
    sh -s -
}

get-cert() {
  local USER="$1"

  sudo kubectl get csr "$USER-k8s-access" \
    -o jsonpath="{.status.certificate}" 2>/dev/null
}

create-user() {
  local USER="$1"

  if [ ! -f "$HOME/.hogarlab/$USER.key" -o \
       ! -f "$HOME/.hogarlab/$USER.csr" ]; then
    echo "Generando certificado para el nuevo usuario ..."
    openssl req -new \
      -newkey rsa:4096 \
      -nodes \
      -keyout "$HOME/.hogarlab/$USER.key" \
      -out "$HOME/.hogarlab/$USER.csr" \
      -subj "/CN=$USER/O=devops"
  fi

  echo "Solicitando firma del certificado  ..."
  local REQUEST=$(cat "$HOME/.hogarlab/$USER.csr" | base64 | tr -d "\n")
  sudo kubectl apply -f - <<EOF
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: $USER-k8s-access
spec:
  groups:
  - system:authenticated
  - system:admin
  request: "$REQUEST"
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
EOF

  until sudo kubectl get csr "$USER-k8s-access" >/dev/null; do
    sleep 0.7
  done

  echo "Aprobando certificado ..."
  sudo kubectl certificate approve "$USER-k8s-access"
  until sudo kubectl get csr "$USER-k8s-access" | grep 'Issued' >/dev/null; do
    sleep 0.7
  done

  echo "Asignando roles de administrador al nuevo usuario ..."
  if ! kubectl get clusterrolebinding "$USER-admin" \
    >/dev/null 2>&1; then

    sudo kubectl create clusterrolebinding "$USER-admin" \
      --clusterrole=cluster-admin --user="$USER"
  fi
}

if [ -z "$1" ]; then
  fail "Necesitas pasar el usuario como parámetro:

  $0 `whoami`
"
fi

echo "Necesitaremos elevar permisos para poder realizar la instalación."
if ! sudo true; then
  fail "No hemos podido autenticarnos como root."
fi

if [ ! -d "$HOME/.hogarlab" ]; then
  mkdir "$HOME/.hogarlab"
fi

USER="$1"

type k3s >/dev/null 2>&1 || install-k3s || fail "La instalación ha fallado."

if [ ! -f "$HOME/.hogarlab/$USER.crt" -o \
     ! -f "$HOME/.hogarlab/$USER.key" ]; then
  echo "Obteniendo par de claves ..."
  CERTIFICATE=$(get-cert "$USER")

  if [ "$?" != "0" ]; then
    create-user "$USER"

    CERTIFICATE=$(get-cert "$USER")
  fi

  echo "$CERTIFICATE" | \
    base64 --decode - > "$HOME/.hogarlab/$USER.crt"
fi

if [ ! -f "$HOME/.hogarlab/k8s-ca.crt" ]; then
  sudo kubectl config view \
    -o jsonpath='{.clusters[0].cluster.certificate-authority-data}' --raw | \
    base64 --decode - > ~/.hogarlab/k8s-ca.crt
fi

if [ ! -f "$HOME/.hogarlab/endpoint.txt" ]; then
  CONTEXT=$(sudo kubectl config current-context)
  NAME=$(sudo kubectl config get-contexts $CONTEXT | awk '{print $3}' | tail -n 1)
  ENDPOINT=$(sudo kubectl config view \
    -o jsonpath="{.clusters[?(@.name == \"$NAME\")].cluster.server}")

  echo "$ENDPOINT" > "$HOME/.hogarlab/endpoint.txt"
fi
